import { Component } from '@angular/core';

@Component({
  selector: 'products-root',
  template: 'Hello Products! <router-outlet></router-outlet>'
})
export class ProductsComponent {
  title = 'products';
}
