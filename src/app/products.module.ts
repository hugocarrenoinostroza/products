import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { ProductsRoutingModule } from './products-routing.module';
import { ProductsComponent } from './products.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    ProductsComponent
  ],
  imports: [
    BrowserModule,
    ProductsRoutingModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [ProductsComponent]
})
export class ProductsModule { }
